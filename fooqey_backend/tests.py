from django.contrib.staticfiles.testing import LiveServerTestCase
from selenium import webdriver
import time
import os


class TestFunctional(LiveServerTestCase):

    @classmethod
    def setUpClass(self):
        super().setUpClass()

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)

    @classmethod
    def tearDownClass(self):
        self.browser.close()
        super().tearDownClass()

    def test_if_working(self):
        self.browser.get(self.live_server_url)
        assert 1 == 1

