from django.shortcuts import render, redirect
from fooqey_backend.models import Keranjang, Riwayat, Barang, TransaksiCepat
import json
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponseBadRequest, HttpResponse
from django.core.serializers import serialize
# Create your views here.

def riwayat_transaksi_cepat_page(request):
    history = Riwayat.objects.all()
    context = {"riwayats": history}
    return render(request, "riwayat_transaksi_cepat.html", context)

def riwayat_transaksi_cepat_load(request, pk):
    history = Riwayat.objects.get(pk= pk)
    data = serialize('json', history.barang.all())
    return JsonResponse(data, safe=False)
@csrf_exempt
def transaksi_cepat_cancel(request, pk):
    riwayat = Riwayat.objects.get(pk= pk)
    for barang in riwayat.barang.all():
        barang.delete()
    riwayat.delete()
    return HttpResponse()

@csrf_exempt
def transaksi_cepat(request):
    carts = Keranjang.objects.all()
    error = False
    total_price = 0
    # konfirmasi
    if request.method == 'GET':
        if carts.count() == 0:
            return JsonResponse({
                'status': 'error',
                'message': 'Your cart is empty.'
            })
        else:
            history = Riwayat.objects.create(
                total_harga=total_price
            )
            for cart in carts:
                transaction_object= TransaksiCepat.objects.create(
                    nama=cart.barang.nama,
                    harga=cart.barang.harga,
                    jumlah=cart.jumlah,
                    subtotal=cart.jumlah * cart.barang.harga

                )
                history.barang.add(transaction_object)
            #cek kupon dan hitung total
            for item in history.barang.all():
                if item.kupon != None:
                    if item.harga > item.kupon.hargamin:
                        item.subtotal = item.subtotal*item.kupon.persen
                total_price += item.subtotal
            history.total_harga=total_price
            history.save()
            pk = history.pk
            data = []
            for item in history.barang.all():
                data.append({
                    'nama':item.nama,
                    'harga':item.harga,
                    'jumlah':item.jumlah,
                    'subtotal':item.subtotal
                })
            json_data = {
                'status': 'success',
                'pk': pk,
                'total': total_price,
                'data' : data
            }
            return JsonResponse( json_data )
    # transaksi
    if request.method == 'POST':
        for cart in carts:
            product = cart.barang
            product.stok -= cart.jumlah
            if product.stok < 0:
                product.stok += cart.jumlah
                error = True
                response_message = product.nama + ' is out of stock.'
                return JsonResponse({
                    'status': 'error',
                    'message': response_message
                })
        if error == False:

            for cart in carts:
                cart.barang.save()
                cart.delete()
            
            return JsonResponse({
                'status': 'success',
                'message': 'Transaction successful.'
            })
    return HttpResponseBadRequest()


  


        