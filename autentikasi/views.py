from django.shortcuts import render
from django.contrib.auth import authenticate, login as authlogin, logout as authlogout
from django.contrib.auth.models import User
from django.shortcuts import redirect


def login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)

        if user is not None:
            print("yayy")
            print(user.username)
            authlogin(request, user)
            return redirect('index')

    return render(request, "autentikasi/login.html")


def logout(request):
    authlogout(request)
    return redirect('/')


def register(request):
    if request.method == "POST":
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']

        try:
            user = User.objects.create_user(username, email, password)
            print(user.username)
        except:
            redirect("register")

        return redirect('login')

    return render(request, "autentikasi/register.html")
