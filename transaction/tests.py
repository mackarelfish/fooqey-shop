from django.test import TestCase

from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

from .models import *
from .views import *
from .apps import *

import time
import os


class UnitTestsBookSearch(TestCase):

    def test_apps(self):
        self.assertEqual(TransactionConfig.name, 'transaction')
        self.assertEqual(apps.get_app_config(
            'transaction').name, 'transaction')

    def test_index_routing_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_index_routing_correct(self):
        function = resolve('/')
        self.assertNotEqual(function.func, transaction)

    def test_index_html_render(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'base.html')


class FunctionalTestBookSearch(LiveServerTestCase):
    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.driver = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.driver = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.driver.quit()
        super(FunctionalTestBookSearch, self).tearDown()

    def test_transaction_history_functionality(self):
        self.driver.get(self.live_server_url)
        transaction = self.driver.find_element_by_class_name("landing-button")
        transaction.click()

        self.assertIn("html", self.driver.page_source)
