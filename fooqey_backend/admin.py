from django.contrib import admin
from .models import Barang, Kupon, Transaksi, Kategori, Ulasan, Riwayat, Keranjang, TransaksiCepat

# Register your models here.
admin.site.register(Barang)
admin.site.register(Kupon)
admin.site.register(Transaksi)
admin.site.register(Kategori)
admin.site.register(Ulasan)
admin.site.register(Riwayat)
admin.site.register(Keranjang)
admin.site.register(TransaksiCepat)
