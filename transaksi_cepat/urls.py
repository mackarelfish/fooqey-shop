from django.urls import path
from . import views

urlpatterns = [
    path("", views.transaksi_cepat, name="transaksi_cepat"),
    path("history/", views.riwayat_transaksi_cepat_page, name="tc_history"),
    path("history/<int:pk>/", views.riwayat_transaksi_cepat_load, name="tc_load"),
    path("cancel/<int:pk>/", views.transaksi_cepat_cancel, name="tc_cancel")
]
