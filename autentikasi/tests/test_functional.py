from django.contrib.staticfiles.testing import LiveServerTestCase
from django.contrib.auth.models import User
from selenium import webdriver
import time
import os


class TestFunctional(LiveServerTestCase):

    @classmethod
    def setUpClass(self):
        super().setUpClass()

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)

        self.user = User.objects.create_user("andi", "andi@andi.com", "budi")

    @classmethod
    def tearDownClass(self):
        self.browser.close()
        super().tearDownClass()

    def test_login_working(self):
        old_url = self.live_server_url + "/auth/login"
        self.browser.get(old_url)
        form = {
            "username": self.browser.find_element_by_id("username"),
            "password": self.browser.find_element_by_id("password"),
            "button": self.browser.find_element_by_id("password")
        }

        form['username'].send_keys("andi")
        form['password'].send_keys("budi")
        form['button'].click()

        self.assertNotEqual(old_url, self.live_server_url)

    def test_register_working(self):
        old_url = self.live_server_url + "/auth/register"
        self.browser.get(old_url)
        form = {
            "username": self.browser.find_element_by_id("username"),
            "email": self.browser.find_element_by_id("email"),
            "password": self.browser.find_element_by_id("password"),
            "button": self.browser.find_element_by_id("password")
        }

        form['username'].send_keys("budi")
        form['password'].send_keys("budi@budi.com")
        form['password'].send_keys("andi")
        form['button'].click()

        self.assertNotEqual(old_url, self.live_server_url)
