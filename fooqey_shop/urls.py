from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include("fooqey_frontend.urls")),
    path('auth/', include("autentikasi.urls")),
    path('product_info/', include("productinfo.urls")),
    path('transaction/', include("transaction.urls")),
    path('wishlist/', include("wishlist.urls")),
    path('ulasan/', include("ulasan.urls")),
    path('transaksi_cepat/', include("transaksi_cepat.urls")),
    path('cart/', include("keranjang.urls"))
]
