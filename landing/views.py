from django.shortcuts import render
from fooqey_backend.models import Barang

# Create your views here.


def index(request):
    getBarang = Barang.objects.all()
    print(request.user.username)
    context = {
        "data": getBarang
    }
    return render(request, "landing/index.html", context)
