from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class RangedIntegerField(models.IntegerField):

    def __init__(self, min_value=None, max_value=None, **kwargs):
        self.min_value = min_value
        self.max_value = max_value
        if 'validators' in kwargs:
            validators = kwargs['validators']
        else:
            validators = []
        if min_value:
            validators.append(MinValueValidator(min_value))
        if max_value:
            validators.append(MaxValueValidator(max_value))
        kwargs['validators'] = validators
        super(RangedIntegerField, self).__init__(**kwargs)

    def formfield(self, **kwargs):
        context = {'min_value': self.min_value, 'max_value': self.max_value}
        context.update(kwargs)
        return super(RangedIntegerField, self).formfield(**context)