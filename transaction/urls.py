from django.urls import path
from . import views

urlpatterns = [
    path("", views.transaction, name="transaction"),
    path("checkout/<int:id_barang>", views.checkout, name="checkout"),
    path("cariKupon/<int:persenTarget>", views.cariKupon, name="cariKupon")
]
