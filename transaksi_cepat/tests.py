from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from django.urls import resolve

import os
import time
import random

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from fooqey_backend.models import Barang, Riwayat, TransaksiCepat
from .views import *
# Create your tests here.


class UnitTest(TestCase):
    def test_Riwayat_create_object(self):
        riwayat = Riwayat.objects.create(total_harga=0)
        for x in range(10):
            nama = "andi"
            harga = random.randint(10000, 50000)
            jumlah = random.randint(1, 100)
            tc = TransaksiCepat.objects.create(
                nama=nama,
                harga=harga,
                jumlah=jumlah,
                subtotal=harga*jumlah
            )
            riwayat.barang.add(tc)

        counter = TransaksiCepat.objects.all().count()
        self.assertEqual(counter, 10)

        counter2 = Riwayat.objects.all().count()
        self.assertEqual(counter2, 1)


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(FunctionalTest, self).setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-popup-blocking')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()

        super(FunctionalTest, self).tearDown()

    def test_transaksi_cepat(self):
        barang = Barang.objects.create(
            nama="selimut",
            harga=20000,
            stok=5,
            deskripsi="keras",
        )
        keranjang = Keranjang.objects.create(
            barang=barang,
            jumlah=3
        )

        self.browser.get(self.live_server_url)
        time.sleep(10)

        self.browser.find_element_by_class_name('btn-modal').click()
        time.sleep(10)
        self.browser.find_element_by_id("transaksi-cepat-load-btn").click()
        time.sleep(10)

        self.assertIn('selimut', self.browser.page_source)

        self.assertIn('selimut', self.browser.page_source)

        self.browser.find_element_by_id("transaksi-cepat-execute-btn").click()

        counter = Riwayat.objects.all().count()
        self.assertEqual(counter, 1)

