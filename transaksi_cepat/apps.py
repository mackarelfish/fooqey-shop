from django.apps import AppConfig


class TransaksiCepatConfig(AppConfig):
    name = 'transaksi_cepat'
