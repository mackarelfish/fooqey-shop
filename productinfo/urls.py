from django.urls import path

from . import views

urlpatterns = [
    path('', views.product_info, name='product_info'),
    path('<int:pkTarget>/', views.product_info, name='product_info')
]
