/*var _wasPageCleanedUp = false;
function pageCleanup(){
    if (!_wasPageCleanedUp)
    {
        $.ajax({
            type: 'POST',
            async: false,
            url: "transaksi_cepat/cancel/"+ pk +"/",
            success: function ()
            {
                _wasPageCleanedUp = true;
            }
        });
    }
}
*/
$(document).ready(function () {
    // Load transaksi cepat
    $('#transaksi-cepat-load-btn').click(function () {
        var pk = 0;
        $.ajax({
            type: "GET",
            url: "transaksi_cepat/",
            beforeSend: function (e){
                pk = 0;
                console.log(e);
                $("#transaction-load-success").addClass("hidden");
                $("#transaction-load-failed").addClass("hidden");
                $("#transaction-success").addClass("hidden");
                $("#transaction-failed").addClass("hidden");
                $("#transaction-process").removeClass("hidden");
                $("#transaksi-cepat-execute-btn").addClass("hidden");
                $("#transaksi-cepat-cancel-btn").html("Cancel");
            },
            complete: function(){
                $("#transaction-process").addClass("hidden");
                console.log("transaksi load completed")
            },
            success: function (response) {
                if (response.status == 'success'){
                    pk = response.pk
                    items = response.data
                    console.log(response);
                    console.log("transaksi load successful");
                    // buat table
                    var res = " <tr> <th>Item</th> <th>Price</th> <th>Quantity</th> <th>Subtotal</th> </tr>"
                    for (i = 0; i < items.length; i++){
                        res+= "<tr> <th>"+ items[i].nama +"</th> <th>"+  items[i].harga
                        +"</th> <th>"+ items[i].jumlah +"</th> <th>"+ items[i].subtotal +"</th> </tr>"
                    }
                    $("#transaksi-table").html(res);
                    $("#total-price").html(response.total);
                    $("#transaksi-cepat-execute-btn").removeClass("hidden");
                    $("#transaction-load-success").removeClass("hidden");
                    
                } else if (response.status == 'error'){
                    console.log(response.status)
                    console.log("transaksi load failed");
                    $("#transaction-load-failed").removeClass("hidden");
                    $('#transaction-load-failed').html(response.message);
                    
                }
            }
        });/*
        // if page is refreshed
        $(window).on('beforeunload', function ()
        {
            //this will work only for Chrome
            pageCleanup();
        });
        
        $(window).on("unload", function ()
        {
            //this will work for other browsers
            pageCleanup();
        });
        */
        // cancel
        $('#transaksi-cepat-cancel-btn').click(function () { 
            if (pk > 0){
                $.ajax({
                    type: "POST",
                    url: "transaksi_cepat/cancel/"+ pk +"/"
                })
            }
        });
    });
    // lakukan transaksi cepat
    $('#transaksi-cepat-execute-btn').click(function () { 
        $.ajax({
            type: "POST",
            url: "transaksi_cepat/",
            beforeSend: function (e){
                console.log(e);
                $("#transaction-load-failed").addClass("hidden");
                $("#transaction-success").addClass("hidden");
                $("#transaction-failed").addClass("hidden");
                $("#transaction-process").removeClass("hidden");
                $("#transaksi-cepat-execute-btn").addClass("hidden");
            },
            complete: function(){
                $("#transaction-process").addClass("hidden");
                console.log("transaksi completed");
            },
            success: function (response) {
                if (response.status == 'success'){
                    console.log("transaksi success");
                    $("#transaction-success").removeClass("hidden");
                    $("#transaction-success").html(response.message);
                    $("#transaksi-cepat-cancel-btn").html("Close");
                } else if (response.status == 'error'){
                    console.log(response.status)
                    console.log("transaksi failed");
                    $("#transaction-load-failed").removeClass("hidden");
                    $('#transaction-load-failed').html(response.message);
                    
                }
            }
        })
    });

});
// load riwayat
$(document).ready(function () {
    $(".riwayat-container").click(function () { 
        console.log("clicked");
        var pk = $(this).attr("id");
        var container = $(this);
        $.ajax({
            type: "GET",
            url: pk +"/",
            beforeSend: function (e){
                console.log(e);
            },
            success: function(data){
                var response = JSON.parse(data);
                console.log(response);
                // buat table
                var res = "<table> <tr> <th>Item</th> <th>Price</th> <th>Quantity</th> <th>Subtotal</th> </tr>"
                for (i = 0; i < response.length; i++){
                    res+= "<tr> <th>"+ response[i].fields.nama +"</th> <th>"+  response[i].fields.harga
                    +"</th> <th>"+ response[i].fields.jumlah +"</th> <th>"+ response[i].fields.subtotal +"</th> </tr>"
                }
                res += "</table>"
                $(container).children(".riwayat-content").html(res);
            }
        }); 
    });
});
