from django import forms

class UlasanForm(forms.Form):
    bintang_choices = (
        (1, "1"),
        (2, "2"),
        (3, "3"),
        (4, "4"),
        (5, "5"),
    )
    nama = forms.CharField(label='nama')
    comment = forms.CharField(label='comment', required=False, 
        widget=forms.TextInput(attrs={'class':'form-control'}))
    bintang = forms.TypedChoiceField(label='bintang', required=False,
        choices = bintang_choices, coerce = int, empty_value= None)