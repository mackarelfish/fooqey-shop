from django.shortcuts import render
from fooqey_backend.models import Ulasan, Barang
from django.template.defaulttags import register

@register.filter(name='get_range')
def get_range(value):
    return range(value)

# Create your views here.
def ulasan (request, pkTarget = 0):
    #Tidak akan menampilkan apapun sebagai default. Untuk mencoba jalannya program, harus memasukkan url dispatcher secara manual karena belum menyambungkan app dengan info barang.
    dataUlasan = Ulasan.objects.filter(pk = pkTarget)
    dataBarang = Barang.objects.filter(pk=pkTarget)
    return render(request, 'ulasan.html', {'data':dataUlasan, 'deskripsi':dataBarang})
