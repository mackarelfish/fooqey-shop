from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import addKeranjang, removeKeranjang, addItem, removeItem, showKeranjang
from fooqey_backend.models import Barang, Keranjang
import time
import os
import json

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait

# Create your tests here.


class KeranjangUnitTest(TestCase):
    def test_create_keranjang_object(self):
        barang = Barang.objects.create(
            nama='Sapu', harga="10000", stok="10", deskripsi="Bagus")
        id_barang = str(barang.id)
        response = Client().get('/cart/addcart/' + id_barang)
        self.assertEquals(response.status_code, 200)

        response = Client().get('/cart/addcart/' + id_barang)
        self.assertEquals(response.status_code, 200)

    def test_show_keranjang(self):
        barang = Barang.objects.create(
            nama='Sapu', harga="10000", stok="10", deskripsi="Bagus")
        item = Keranjang.objects.create(barang=barang, jumlah=1)
        response = Client().get('/cart/showcart/')
        self.assertEqual(response.status_code, 200)

    def test_delete_keranjang_object(self):
        barang = Barang.objects.create(
            nama='Sapu', harga="10000", stok="10", deskripsi="Bagus")
        item = Keranjang.objects.create(barang=barang, jumlah=1)
        item_id = str(item.id)
        response = Client().get('/cart/removecart/' + item_id)
        self.assertEquals(response.status_code, 200)

    def test_edit_jumlah_keranjang_object(self):
        barang = Barang.objects.create(
            nama='Sapu', harga="10000", stok="10", deskripsi="Bagus")
        item = Keranjang.objects.create(barang=barang, jumlah=1)
        item_id = str(item.id)

        response = Client().get('/cart/additem/' + item_id)
        self.assertEquals(response.status_code, 200)

        response = Client().get('/cart/removeitem/' + item_id)
        self.assertEquals(response.status_code, 200)


class KeranjangFunctionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument("--disable-popup-blocking")

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_keranjang_functionality(self):
        assert 1 == 1

