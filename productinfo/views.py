from django.shortcuts import render, redirect
from fooqey_backend.models import Ulasan, Barang
from .forms import UlasanForm
from django.template.defaulttags import register

@register.filter (name='get_range')
def get_range(value):
   return range(value)


def product_info(request, pkTarget = 0):
    form = UlasanForm()
    produk = Barang.objects.filter(pk = pkTarget).first()

    if not produk:
        return redirect('/')
        
    if request.method == 'POST':
        form = UlasanForm(request.POST or None, request.FILES or None)
        response_data = {}

        if form.is_valid():

            bar = Barang.objects.filter(pk=request.POST['id']).first()
            print(bar)

            response_data['bintang'] = request.POST['bintang']
            response_data['nama'] = request.POST['nama']
            response_data['comment'] = request.POST['comment']


            data_jadwal = Ulasan(pesan=response_data['comment'],
                                    bintang=int(response_data['bintang']),
                                    nama=response_data['nama'], barang=bar)
            data_jadwal.save()
            return redirect(request.path_info)
        else:
            return render(request, 'productinfo/InfoBarang.html', {'form': form, 'produk' : produk})
    
    else:
        return render(request, 'productinfo/InfoBarang.html', {'form': form, 'produk' : produk})



    
