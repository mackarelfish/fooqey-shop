from django.db import models
from .customfields import RangedIntegerField


class Kategori(models.Model):
    nama = models.CharField(max_length=20)


class Barang(models.Model):
    nama = models.CharField(max_length=30)
    harga = models.IntegerField()
    stok = models.IntegerField()
    deskripsi = models.TextField()
    kategori = models.OneToOneField(
        Kategori,
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )


class Kupon(models.Model):
    kode = models.CharField(max_length=10)
    persen = models.IntegerField()
    tanggal = models.DateTimeField(null=True,
                                   blank=True)
    hargamin = models.IntegerField(null=True,
                                   blank=True)

    def __str__(self):
        return self.kode


class Transaksi(models.Model):
    pembeli = models.CharField(max_length=20)
    total = models.IntegerField()
    tanggal = models.DateTimeField(auto_now_add=True)
    Barang = models.ForeignKey(
        Barang,
        on_delete=models.SET_NULL,
        null=True
    )
    kupon = models.ForeignKey(
        Kupon,
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )


class Ulasan(models.Model):
    nama = models.CharField(max_length=20)
    bintang = RangedIntegerField(min_value=1, max_value=5)
    pesan = models.TextField()
    tanggal = models.DateTimeField(auto_now_add=True)
    barang = models.ForeignKey(
        Barang,
        on_delete=models.CASCADE,
        null=True
    )


class Keranjang(models.Model):
    barang = models.ForeignKey(
        Barang,
        on_delete=models.SET_NULL,
        null=True
    )
    jumlah = models.IntegerField(default=1)


class TransaksiCepat(models.Model):
    nama = models.CharField(max_length=30)
    harga = models.IntegerField()
    jumlah = models.IntegerField()
    subtotal = models.IntegerField()
    kupon = models.ForeignKey(
        Kupon,
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )


class Riwayat(models.Model):
    tanggal = models.DateTimeField(auto_now_add=True)
    barang = models.ManyToManyField(TransaksiCepat)
    total_harga = models.IntegerField()

