from django import forms

class CheckoutForm(forms.Form):
    namaPembeli = forms.CharField(max_length=20)
    kodeKupon = forms.CharField(max_length=10, required=False)
    
