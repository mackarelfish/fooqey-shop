from django.apps import AppConfig


class FooqeyBackendConfig(AppConfig):
    name = 'fooqey_backend'
