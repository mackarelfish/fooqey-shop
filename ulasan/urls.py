from django.urls import path
from . import views

urlpatterns = [
    path("", views.ulasan, name="ulasan"),
    path("<int:pkTarget>/", views.ulasan, name="ulasan"),
]
