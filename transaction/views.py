from django.shortcuts import render
from django.shortcuts import redirect
from fooqey_backend.models import Transaksi, Kupon, Barang
from .forms import CheckoutForm

from django.http import JsonResponse
from django.core import serializers


# Create your views here.

def transaction(request):
    transactions = Transaksi.objects.all()
    subtotal_str = ""
    for transaction in transactions:
        subtotal = int(transaction.total)
        subtotal_str = "Rp " + str(subtotal)

    context = {
        'transactions' : transactions,
        'subtotal' : subtotal_str
    }

    return render(request, "transaction/transaction.html", context)

def checkout(request, id_barang):
    barang1 = {}
    try:
        barang1 = Barang.objects.get(pk=id_barang)
    except:
        return redirect('/')
    if request.method == "POST":
        form = CheckoutForm(request.POST)

        if form.is_valid():
            kodeKupon = form.cleaned_data.get('kodeKupon')
            namaPembeli = form.cleaned_data.get('namaPembeli')
            coupon = Kupon.objects.filter(kode=kodeKupon).first()
            if coupon:
                subtotal = int(barang1.harga * (100 - coupon.persen) / 100)
                checkout = Transaksi.objects.create(pembeli = namaPembeli, total = subtotal, kupon = coupon, Barang = barang1)
            else:
                subtotal = int(barang1.harga)
                checkout = Transaksi.objects.create(pembeli = namaPembeli, total = subtotal, Barang = barang1)

            return redirect('/')
    else:
        form = CheckoutForm()

    return render(request, "transaction/checkout.html", {'form' : form, 'barang1' : barang1})

def cariKupon(request, persenTarget):
    kupon = Kupon.objects.filter(persen = persenTarget).values()
    return JsonResponse({"kuponList":list(kupon)})