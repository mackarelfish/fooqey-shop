$(document).ready(function() {
    $('.btn-addtocart').click(function(){
        console.log("click success");
        var id = $(this).attr('id');

        var xhttpr = new XMLHttpRequest();
        xhttpr.onreadystatechange = function() {
            if(this.readyState == 4){
                if(this.status == 200){
                    console.log(id);
                    addKeranjang(id);
                    console.log("addKeranjang executed");

                } else {
                    alert("Maaf, barang belum berhasil ditambahkan ke keranjang")
                }
            } 
        }
        xhttpr.open("GET","/");
        xhttpr.send();
    });
});

//Menambahkan barang ke keranjang
function addKeranjang(id){
    console.log("masuk addKeranjang AJAX")
    $.ajax({
        url: "/cart/addcart/" + id,
        success: function(data){
            console.log('item', data);
            alert(data.nama + " berhasil ditambahkan ke keranjang");
        }
     });

}

//Menampilkan keranjang
function showKeranjang(id){
    console.log("masuk showKeranjang AJAX")
    $.ajax({
        url: "/cart/showcart/",
        success: function(data){
            console.log('keranjang', data);
            var result="";

            for (var i=0; i<data.length; i++){
            result += "<div class='card keranjang-card mb-4' style='width: 25rem;'>" + 
                "<div class='card-body'>" + 
                "<h5 class='card-title'>" + data[i].nama + "</h5>" + 
                "<h6 class='card-subtitle mb-2 text-muted'>Rp. " + data[i].harga + "</h6>" + 
                "<p class='card-text'>" + 
                "<img class='btn-removeItem' style='max-width:24px;' onclick='removeItem(" + data[i].id + ")' src='https://img.icons8.com/ios/64/000000/minus.png'/>" + 
                "<span class='amount-" + data[i].id + " mx-3'>" + data[i].jumlah + "</span>" +
                "<img class='btn-addItem' style='max-width:24px;' onclick='addItem(" + data[i].id + ")' src='https://img.icons8.com/ios/64/000000/plus.png'/>" +
                "</p>" + 
                "<button type='button' class='btn btn-primary btn-removeKeranjang' onclick='removeKeranjang(" + data[i].id + ")'>REMOVE</button>" +
                "</div></div>"
            
            }
            $('.cart-modal').html(result);
            
        }
     });

}

//Menghapus item dari keranjang
function removeKeranjang(id){
    console.log("masuk removeKeranjang")
    let confirmation = confirm("are you sure you want to remove the item?");
    if (confirmation) {
        $.ajax({
           url: "/cart/removecart/" + id,
           success: function(data){
               console.log('item deleted');
               alert(data.nama + " telah dihapus dari keranjang belanja")
               showKeranjang()
           }
        });
    }
}

//Menambahkan jumlah barang x yang telah ada di keranjang
function addItem(id){
    console.log("masuk addItem")
    $.ajax({
        url: "/cart/additem/" + id,
        success: function(data){
            console.log('added jumlah item');
            $('.amount-' + id).html(data.jumlah);
        }
     });

}

//Mengurangi jumlah barang x yang telah ada di keranjang
function removeItem(id){
    console.log("masuk removeItem")
    $.ajax({
        url: "/cart/removeitem/" + id,
        success: function(data){
            console.log('removed jumlah item');
            $('.amount-' + id).html(data.jumlah);
        }
     });

}