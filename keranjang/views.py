from django.shortcuts import render
from django.shortcuts import redirect
from fooqey_backend.models import Transaksi, Kupon, Barang, Keranjang
import json
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

#Create your views here.

def addKeranjang(request, id_barang):
    try: 
        keranjang = Keranjang.objects.filter(barang_id=id_barang)[:1].get()
        keranjang.jumlah +=1
        keranjang.save()
        
    
    except Keranjang.DoesNotExist:
        barang1 = Barang.objects.get(pk=id_barang)
        keranjang = Keranjang.objects.create(barang = barang1, jumlah=1)

    return JsonResponse({'nama': keranjang.barang.nama})

def showKeranjang(request):
    lst_items=[]
    items = Keranjang.objects.all()

    for item in items:
        lst_items.append({
            'id' : item.id,
            'nama': item.barang.nama,
            'harga': item.barang.harga,
            'deskripsi': item.barang.deskripsi,
            'jumlah': item.jumlah, 
        })

    return JsonResponse(lst_items, safe=False)

def removeKeranjang(request, id_keranjang):
    item = Keranjang.objects.get(pk=id_keranjang)
    nama = item.barang.nama
    item.delete()
    return JsonResponse({'nama' : nama})

def addItem(request, id_keranjang):
    item = Keranjang.objects.filter(pk=id_keranjang)[:1].get()
    print(item)
    item.jumlah +=1
    item.save()

    return JsonResponse({'jumlah' : item.jumlah})

def removeItem(request, id_keranjang):
    item = Keranjang.objects.filter(pk=id_keranjang)[:1].get()
    print(item)
    item.jumlah -=1
    item.save()

    return JsonResponse({'jumlah' : item.jumlah})