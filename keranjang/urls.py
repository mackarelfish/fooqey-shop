from django.urls import path
from . import views

urlpatterns = [
    path("addcart/<int:id_barang>", views.addKeranjang, name="addcart"),
    path("showcart/", views.showKeranjang, name="showcart"),
    path("removecart/<int:id_keranjang>", views.removeKeranjang, name="removeFromCart"),
    path("additem/<int:id_keranjang>", views.addItem, name="additem"),
    path("removeitem/<int:id_keranjang>", views.removeItem, name="removeitem")
]