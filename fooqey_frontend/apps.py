from django.apps import AppConfig


class FooqeyFrontendConfig(AppConfig):
    name = 'fooqey_frontend'
